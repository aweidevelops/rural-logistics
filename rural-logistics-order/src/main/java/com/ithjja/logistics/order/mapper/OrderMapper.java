package com.ithjja.logistics.order.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ithjja.logistics.order.model.Order;

/**
 * @author hejja
 */
@Mapper
public interface OrderMapper {

    /**
     * 添加订单
     * @param order 订单信息
     */
    @Insert("insert into tbl_order values(#{id}, #{uid}, #{startPlace}, #{endPlace}, #{cropsIds}, #{driverId}, #{status}, #{remarks}, "
        + " #{price}, #{createTime}, #{orderTime}, #{deleted})")
    @Options(useGeneratedKeys = true, keyProperty="id", keyColumn="id")
    void addOrder(Order order);

    /**
     * 查询所有订单
     * @return 订单集合
     */
    @Select("select * from tbl_order order by create_time desc")
    @Results(id = "defaultResultMapping", value = {
        @Result(id = true, column = "id", property = "id"),
        @Result(column = "uid", property = "uid"),
        @Result(column = "start_place", property = "startPlace"),
        @Result(column = "end_place", property = "endPlace"),
        @Result(column = "crops_ids", property = "cropsIds"),
        @Result(column = "driver_id", property = "driverId"),
        @Result(column = "status", property = "status"),
        @Result(column = "remarks", property = "remarks"),
        @Result(column = "price", property = "price"),
        @Result(column = "create_time", property = "createTime"),
        @Result(column = "order_time", property = "orderTime"),
        @Result(column = "deleted", property = "deleted")
    })
    List<Order> findAll();

    /**
     * 根据用户id查询订单
     * @param uid 用户id
     * @return 订单集合
     */
    @Select("select * from tbl_order where uid = #{uid}")
    @ResultMap("defaultResultMapping")
    List<Order> findByUid(@Param("uid") Long uid);

    /**
     * 根据司机id查询订单
     * @param driverId 司机id
     * @return 订单集合
     */
    @Select("select * from tbl_order where driver_id = #{driverId}")
    @ResultMap("defaultResultMapping")
    List<Order> findByDriverId(@Param("driverId") Long driverId);

    /**
     * 查询订单总数量
     * @return 订单数
     */
    @Select("select count(*) from tbl_order")
    Long findOrderTotal();

    /**
     * 根据状态查询订单数量
     * @param status 状态
     * @return 订单数量
     */
    @Select("select count(*) from tbl_order where status = #{status}")
    Long findOrderTotalByStatus(@Param("status") Integer status);

    /**
     * 根据用户id查询订单数量
     * @param uid 用户id
     * @return 订单数量
     */
    @Select("select count(*) from tbl_order where uid = #{uid}")
    Long findOrderTotalByUid(@Param("uid") Long uid);

    /**
     * 根据用户id与订单状态查询订单数量
     * @param uid 用户id
     * @param status 订单状态
     * @return 订单数量
     */
    @Select("select count(*) from tbl_order where uid = #{uid} and status = #{status}")
    Long findOrderTotalByUidAndStatus(@Param("uid") Long uid, @Param("status") Integer status);

    /**
     * 根据司机id查询订单数量
     * @param driverId 用户id
     * @return 订单数量
     */
    @Select("select count(*) from tbl_order where driver_id = #{driverId}")
    Long findOrderTotalByDriverId(@Param("driverId") Long driverId);

    /**
     * 根据司机id与订单状态查询订单数量
     * @param driverId 司机id
     * @param status 订单状态
     * @return 订单数量
     */
    @Select("select count(*) from tbl_order where driver_id = #{driverId} and status = #{status}")
    Long findOrderTotalByDriverIdAndStatus(@Param("driverId") Long driverId, @Param("status") Integer status);

    /**
     * 查询订单总价
     * @return 总价
     */
    @Select("select sum(price) from tbl_order")
    Double findOrderPriceTotal();

    /**
     * 根据订单状态查询订单总价
     * @param status 订单总价
     * @return 总价
     */
    @Select("select sum(price) from tbl_order where status = #{status}")
    Double findOrderPriceTotalByStatus(@Param("status") Integer status);

    /**
     * 根据用户id查询订单总价
     * @param uid 用户id
     * @return 总价
     */
    @Select("select sum(price) from tbl_order where uid = #{uid}")
    Double findOrderPriceByUid(@Param("uid") Long uid);

    /**
     * 根据用户id与订单状态查询订单总价
     * @param uid 用户id
     * @param status 订单状态
     * @return 总价
     */
    @Select("select sum(price) from tbl_order where uid =#{uid} and status = #{status}")
    Double findOrderPriceTotalByUidAndStatus(@Param("uid") Long uid, @Param("status") Long status);

    /**
     * 根据司机id查询订单总价
     * @param driverId 司机id
     * @return 总价
     */
    @Select("select sum(price) from tbl_order where driver_id = #{driverId}")
    Double findOrderPriceTotalByDriverId(@Param("driverId") Long driverId);

    /**
     * 根据司机id与订单状态查询订单总价
     * @param driverId 司机id
     * @param status 订单状态
     * @return 总价
     */
    @Select("select sum(price) from tbl_order where driver_id = #{driverId} and status = #{status}")
    Double findOrderPriceTotalByDriverIdAndStatus(@Param("driverId") Long driverId, @Param("status") Integer status);

    @ResultMap("defaultResultMapping")
    @Select("select * from tbl_order where id = #{id}")
    Order findById(@Param("id") Long id);

    @Update("update tbl_order set crops_ids = #{crops}, status = 2, uid = #{uid}, order_time = #{orderTime} where id = #{id}")
    void updateCrops(@Param("id") Long id, @Param("crops") String crops, @Param("uid") Long uid,
         @Param("orderTime") Date orderTime);

    @Update("update tbl_order set status = #{status}, driver_id = #{driverId}, order_time = #{orderTime} where id = #{id}")
    void updateStatus(@Param("id") Long id, @Param("status") Integer status, @Param("driverId") Long driverId,
          @Param("orderTime") Date orderTime);
}
