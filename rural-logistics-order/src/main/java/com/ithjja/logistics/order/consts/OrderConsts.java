package com.ithjja.logistics.order.consts;

/**
 * @author hejja
 */
public class OrderConsts {

    /** 订单状态：未支付 */
    public static final Integer ORDER_STATUS_UNPAID = 1;

    /** 订单状态：已支付 */
    public static final Integer ORDER_STATUS_PAID = 2;

    /** 订单状态：等等 */
    public static final Integer ORDER_STATUS_3 = 3;

}
