package com.ithjja.logistics.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.ithjja.logistics.common.model.City;
import com.ithjja.logistics.common.model.Province;
import com.ithjja.logistics.common.model.Town;
import com.ithjja.logistics.web.config.FeignConfig;

/**
 * @author hejja
 */
@FeignClient(name = "route", path = "/route", contextId = "order3", configuration = FeignConfig.class)
public interface RouteFeign {

    @GetMapping("/api/province/findById/{id}")
    ResponseEntity<Province> findProvinceById(@PathVariable("id") Long id);

    @GetMapping("/api/city/findById/{id}")
    ResponseEntity<City> findCityById(@PathVariable("id") Long id);

    @GetMapping("/api/town/findById/{id}")
    ResponseEntity<Town> findTownById(@PathVariable("id") Long id);
}
