package com.ithjja.logistics.order.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ithjja.logistics.order.model.Order;
import com.ithjja.logistics.order.service.OrderService;
import com.ithjja.logistics.web.utils.WebUtils;

/**
 * @author hejja
 */
@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 添加订单
     * @param order 订单信息
     * @return 响应
     */
    @PutMapping("/add")
    public ResponseEntity<Long> addOrder(@RequestBody Order order) {
        orderService.addOrder(order);
        return ResponseEntity.ok(order.getId());
    }

    /**
     * 添加订单
     * @param order 订单信息
     * @return 响应
     */
    @PutMapping("/addByDriver")
    public ResponseEntity<Long> addByDriver(@RequestBody Order order) {
        orderService.addByDriver(order);
        return ResponseEntity.ok(order.getId());
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<Order>> findAll() {
        return ResponseEntity.ok(orderService.findAll());
    }

    @GetMapping("/findByUser")
    public ResponseEntity<List<Order>> findByUser() {
        return ResponseEntity.ok(orderService.findByUid(WebUtils.getUserId()));
    }

    @GetMapping("/findByDriver")
    public ResponseEntity<List<Order>> findByDriver() {
        return ResponseEntity.ok(orderService.findByDriverId(WebUtils.getUserId()));
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Order> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(orderService.findById(id));
    }

    @PutMapping("/saveCrops/{id}/{crops}")
    public ResponseEntity<?> saveCrops(@PathVariable("id") Long id, @PathVariable("crops") String crops) {
        orderService.updateCrops(id, crops);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/updateStatus/{id}/{status}")
    public ResponseEntity<?> updateStatus(@PathVariable("id") Long id, @PathVariable("status") Integer status) {
        orderService.updateStatus(id, status);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/findOrderTotalByUid")
    public ResponseEntity<Long> findOrderTotalByUid() {
        return ResponseEntity.ok(orderService.findOrderTotalByUid(WebUtils.getUserId()));
    }

    @GetMapping("/findOrderTotalByUidAndStatus/{status}")
    public ResponseEntity<Long> findOrderTotalByUidAndStatus(@PathVariable("status") Integer status) {
        return ResponseEntity.ok(orderService.findOrderTotalByUidAndStatus(WebUtils.getUserId(), status));
    }

    @GetMapping("/findOrderPriceByUid")
    public ResponseEntity<Double> findOrderPriceByUid() {
        return ResponseEntity.ok(orderService.findOrderPriceByUid(WebUtils.getUserId()));
    }

    @GetMapping("/findOrderPriceTotalByUidAndStatus/{status}")
    public ResponseEntity<Double> findOrderPriceTotalByUidAndStatus(@PathVariable("status") Long status) {
        return ResponseEntity.ok(orderService.findOrderPriceTotalByUidAndStatus(WebUtils.getUserId(), status));
    }

    @GetMapping("/findOrderTotalByDriverId")
    public ResponseEntity<Long> findOrderTotalByDriverId() {
        return ResponseEntity.ok(orderService.findOrderTotalByDriverId(WebUtils.getUserId()));
    }

    @GetMapping("/findOrderTotalByDriverIdAndStatus/{status}")
    public ResponseEntity<Long> findOrderTotalByDriverIdAndStatus(@PathVariable("status") Integer status) {
        return ResponseEntity.ok(orderService.findOrderTotalByDriverIdAndStatus(WebUtils.getUserId(), status));
    }

    @GetMapping("/findOrderPriceTotalByDriverId")
    public ResponseEntity<Double> findOrderPriceTotalByDriverId() {
        return ResponseEntity.ok(orderService.findOrderPriceTotalByDriverId(WebUtils.getUserId()));
    }
}
