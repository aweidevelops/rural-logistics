package com.ithjja.logistics.order.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.ithjja.logistics.common.model.Crops;
import com.ithjja.logistics.common.model.Document;
import com.ithjja.logistics.common.model.User;

/**
 * @author hejja
 */
@Data
public class Order implements Serializable {

    private static final long serialVersionUID = -572229826904628134L;

    private Long id;

    private Long uid;

    private User user;

    private String startPlace;

    private String startPlaceStr;

    private String endPlace;

    private String endPlaceStr;

    private Long driverId;

    private User driver;

    private String cropsIds;

    private List<Crops> cropsList;

    private Integer status;

    private String remarks;

    private Double price;

    private Date createTime;

    private Date orderTime;

    private Boolean deleted = Boolean.FALSE;

    private List<Document> documents;
}
