package com.ithjja.logistics.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.ithjja.logistics.common.model.User;
import com.ithjja.logistics.web.config.FeignConfig;

/**
 * @author hejja
 */
@FeignClient(name = "oauth", path = "/auth", contextId = "order", configuration = FeignConfig.class)
public interface UserFeign {

    @GetMapping("/api/user/getUserInfo/{id}")
    ResponseEntity<User> getUserInfo(@PathVariable("id") Long id);
}
