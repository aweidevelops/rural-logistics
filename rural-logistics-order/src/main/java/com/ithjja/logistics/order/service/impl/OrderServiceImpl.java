package com.ithjja.logistics.order.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjja.logistics.common.consts.DocumentConsts;
import com.ithjja.logistics.common.consts.ModuleConsts;
import com.ithjja.logistics.common.ex.ExpectedException;
import com.ithjja.logistics.common.model.Crops;
import com.ithjja.logistics.order.consts.OrderConsts;
import com.ithjja.logistics.order.feign.CropsFeign;
import com.ithjja.logistics.order.feign.DocumentFeign;
import com.ithjja.logistics.order.feign.RouteFeign;
import com.ithjja.logistics.order.feign.UserFeign;
import com.ithjja.logistics.order.mapper.OrderMapper;
import com.ithjja.logistics.order.model.Order;
import com.ithjja.logistics.order.service.OrderService;
import com.ithjja.logistics.web.utils.WebUtils;

/**
 * @author hejja
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private UserFeign userFeign;

    @Autowired
    private CropsFeign cropsFeign;

    @Autowired
    private RouteFeign routeFeign;

    @Autowired
    private DocumentFeign documentFeign;

    @Override
    public void addOrder(Order order) {
        order.setUid(WebUtils.getUserId());
        order.setCreateTime(new Date());
        order.setStatus(OrderConsts.ORDER_STATUS_UNPAID);
        orderMapper.addOrder(order);
    }

    @Override
    public void addByDriver(Order order) {
        order.setDriverId(WebUtils.getUserId());
        order.setCreateTime(new Date());
        order.setStatus(OrderConsts.ORDER_STATUS_3);
        orderMapper.addOrder(order);
    }

    @Override
    public List<Order> findAll() {
        List<Order> orders = orderMapper.findAll();
        assembleParams(orders);
        return orders;
    }

    @Override
    public List<Order> findByUid(Long uid) {
        List<Order> orders = orderMapper.findByUid(uid);
        assembleParams(orders);
        return orders;
    }

    private void assembleParams(List<Order> orders) {
        for (Order order : orders) {
            assembleOrderParams(order, true);
        }
    }

    private void assembleOrderParams(Order order, boolean isDocuments) {
        List<Long> cropsIds = new ArrayList<>();
        if (order.getDriverId() != null) {
            order.setDriver(userFeign.getUserInfo(order.getDriverId()).getBody());
        }
        if (order.getUid() != null) {
            order.setUser(userFeign.getUserInfo(order.getUid()).getBody());
        }
        if (order.getCropsIds() != null && !order.getCropsIds().isEmpty()) {
            String[] split = order.getCropsIds().split("-");
            for (String s : split) {
                cropsIds.add(Long.valueOf(s));
            }
            List<Crops> body = cropsFeign.findName(cropsIds).getBody();
            order.setCropsList(body);
        }
        order.setStartPlaceStr(assemblePlace(order.getStartPlace()));
        order.setEndPlaceStr(assemblePlace(order.getEndPlace()));
        if (isDocuments) {
            findDocument(order);
        }
    }

    private void findDocument(Order order) {
        order.setDocuments(documentFeign.findDocument(ModuleConsts.CROPS_MODULE,
            DocumentConsts.CROPS_TYPE, order.getId()).getBody());
    }

    /**
     * 组装省份、城市、城镇名
     * @param place 组装前的ids
     * @return 组装后的名称
     */
    private String assemblePlace(String place) {
        String[] split = place.split("-");
        List<String> names = new ArrayList<>();
        if (split.length >= 1) {
            // 省份名
            names.add(routeFeign.findProvinceById(Long.valueOf(split[0])).getBody().getName());
        }
        if (split.length >= 2) {
            // 城市名
            names.add(routeFeign.findCityById(Long.valueOf(split[1])).getBody().getName());
        }
        if (split.length >= 3) {
            // 城镇名
            names.add(routeFeign.findTownById(Long.valueOf(split[2])).getBody().getName());
        }
        return StringUtils.join(names, "-");
    }

    @Override
    public List<Order> findByDriverId(Long driverId) {
        List<Order> orders = orderMapper.findByDriverId(driverId);
        assembleParams(orders);
        return orders;
    }

    @Override
    public Long findOrderTotal() {
        return orderMapper.findOrderTotal();
    }

    @Override
    public Long findOrderTotalByStatus(Integer status) {
        return orderMapper.findOrderTotalByStatus(status);
    }

    @Override
    public Long findOrderTotalByUid(Long uid) {
        return orderMapper.findOrderTotalByUid(uid);
    }

    @Override
    public Long findOrderTotalByUidAndStatus(Long uid, Integer status) {
        return orderMapper.findOrderTotalByUidAndStatus(uid, status);
    }

    @Override
    public Long findOrderTotalByDriverId(Long driverId) {
        return orderMapper.findOrderTotalByDriverId(driverId);
    }

    @Override
    public Long findOrderTotalByDriverIdAndStatus(Long driverId, Integer status) {
        return orderMapper.findOrderTotalByDriverIdAndStatus(driverId, status);
    }

    @Override
    public Double findOrderPriceTotal() {
        return orderMapper.findOrderPriceTotal();
    }

    @Override
    public Double findOrderPriceTotalByStatus(Integer status) {
        return orderMapper.findOrderPriceTotalByStatus(status);
    }

    @Override
    public Double findOrderPriceByUid(Long uid) {
        return orderMapper.findOrderPriceByUid(uid);
    }

    @Override
    public Double findOrderPriceTotalByUidAndStatus(Long uid, Long status) {
        return orderMapper.findOrderPriceTotalByUidAndStatus(uid, status);
    }

    @Override
    public Double findOrderPriceTotalByDriverId(Long driverId) {
        return orderMapper.findOrderPriceTotalByDriverId(driverId);
    }

    @Override
    public Double findOrderPriceTotalByDriverIdAndStatus(Long driverId, Integer status) {
        return orderMapper.findOrderPriceTotalByDriverIdAndStatus(driverId, status);
    }

    @Override
    public Order findById(Long id) {
        Order order = orderMapper.findById(id);
        assembleOrderParams(order, false);
        return order;
    }

    @Override
    public void updateCrops(Long id, String crops) {
        orderMapper.updateCrops(id, crops, WebUtils.getUserId(), new Date());
    }

    @Override
    public void updateStatus(Long id, Integer status) {
        orderMapper.updateStatus(id, status, WebUtils.getUserId(), new Date());
    }
}
