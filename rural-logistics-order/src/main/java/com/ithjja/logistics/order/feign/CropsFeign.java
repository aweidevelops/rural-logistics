package com.ithjja.logistics.order.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.ithjja.logistics.common.model.Crops;
import com.ithjja.logistics.web.config.FeignConfig;

/**
 * @author hejja
 */
@FeignClient(name = "crops", path = "/crops", contextId = "order2", configuration = FeignConfig.class)
public interface CropsFeign {

    @PostMapping("/api/crops/findCrops")
    ResponseEntity<List<Crops>> findName(@RequestBody List<Long> ids);
}
