package com.ithjja.logistics.order.service;

import java.util.List;

import com.ithjja.logistics.order.model.Order;

/**
 * @author hejja
 */
public interface OrderService {

    /**
     * 添加订单
     * @param order 订单信息
     */
    void addOrder(Order order);

    /**
     * 查询所有订单
     * @return 订单集合
     */
    List<Order> findAll();

    /**
     * 根据用户id查询订单
     * @param uid 用户id
     * @return 订单集合
     */
    List<Order> findByUid(Long uid);

    /**
     * 根据司机id查询订单
     * @param driverId 司机id
     * @return 订单集合
     */
    List<Order> findByDriverId(Long driverId);

    /**
     * 查询订单总数量
     * @return 订单数
     */
    Long findOrderTotal();

    /**
     * 根据状态查询订单数量
     * @param status 状态
     * @return 订单数量
     */
    Long findOrderTotalByStatus(Integer status);

    /**
     * 根据用户id查询订单数量
     * @param uid 用户id
     * @return 订单数量
     */
    Long findOrderTotalByUid(Long uid);

    /**
     * 根据用户id与订单状态查询订单数量
     * @param uid 用户id
     * @param status 订单状态
     * @return 订单数量
     */
    Long findOrderTotalByUidAndStatus(Long uid, Integer status);

    /**
     * 根据司机id查询订单数量
     * @param driverId 用户id
     * @return 订单数量
     */
    Long findOrderTotalByDriverId(Long driverId);

    /**
     * 根据司机id与订单状态查询订单数量
     * @param driverId 司机id
     * @param status 订单状态
     * @return 订单数量
     */
    Long findOrderTotalByDriverIdAndStatus(Long driverId, Integer status);

    /**
     * 查询订单总价
     * @return 总价
     */
    Double findOrderPriceTotal();

    /**
     * 根据订单状态查询订单总价
     * @param status 订单总价
     * @return 总价
     */
    Double findOrderPriceTotalByStatus(Integer status);

    /**
     * 根据用户id查询订单总价
     * @param uid 用户id
     * @return 总价
     */
    Double findOrderPriceByUid(Long uid);

    /**
     * 根据用户id与订单状态查询订单总价
     * @param uid 用户id
     * @param status 订单状态
     * @return 总价
     */
    Double findOrderPriceTotalByUidAndStatus(Long uid, Long status);

    /**
     * 根据司机id查询订单总价
     * @param driverId 司机id
     * @return 总价
     */
    Double findOrderPriceTotalByDriverId(Long driverId);

    /**
     * 根据司机id与订单状态查询订单总价
     * @param driverId 司机id
     * @param status 订单状态
     * @return 总价
     */
    Double findOrderPriceTotalByDriverIdAndStatus(Long driverId, Integer status);

    Order findById(Long id);

    void updateCrops(Long id, String crops);

    void updateStatus(Long id, Integer status);

    void addByDriver(Order order);
}
