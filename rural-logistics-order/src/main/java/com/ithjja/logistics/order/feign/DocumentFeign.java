package com.ithjja.logistics.order.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.ithjja.logistics.common.model.Document;
import com.ithjja.logistics.web.config.FeignConfig;

/**
 * @author hejja
 */
@FeignClient(name = "document", path = "/document", contextId = "order4", configuration = FeignConfig.class)
public interface DocumentFeign {

    @GetMapping("/api/document/find/{module}/{type}/{entity}")
    ResponseEntity<List<Document>> findDocument(@PathVariable("module") Integer module,
        @PathVariable("type") Integer type, @PathVariable("entity") Long entity);
}
