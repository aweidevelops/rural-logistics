package com.ithjja.logistics.document.service;

import java.util.List;

import com.ithjja.logistics.document.model.Document;

/**
 * @author hejja
 */
public interface DocumentService {

    /**
     * 添加文件
     * @param documents 文件
     */
    void add(List<Document> documents);

    /**
     * 根据模块、类型、实体查询文件
     * @param module 模块
     * @param type 类型
     * @param entity 实体id
     * @return 文件集合
     */
    List<Document> findByModuleAndTypeAndEntity(Integer module, Integer type,  Long entity);
}
