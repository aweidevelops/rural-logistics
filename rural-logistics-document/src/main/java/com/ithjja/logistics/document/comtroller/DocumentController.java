package com.ithjja.logistics.document.comtroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ithjja.logistics.document.model.Document;
import com.ithjja.logistics.document.service.DocumentService;

/**
 * @author hejja
 */
@RestController
@RequestMapping("/api/document")
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @GetMapping("/find/{module}/{type}/{entity}")
    public ResponseEntity<List<Document>> findDocument(@PathVariable("module") Integer module,
        @PathVariable("type") Integer type, @PathVariable("entity") Long entity) {
        return ResponseEntity.ok(documentService.findByModuleAndTypeAndEntity(module, type, entity));
    }

    @PutMapping("/add")
    public ResponseEntity<?> add(@RequestBody List<Document> documents) {
        documentService.add(documents);
        return ResponseEntity.noContent().build();
    }
}
