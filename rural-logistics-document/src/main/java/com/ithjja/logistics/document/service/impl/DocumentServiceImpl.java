package com.ithjja.logistics.document.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjja.logistics.document.mapper.DocumentMapper;
import com.ithjja.logistics.document.model.Document;
import com.ithjja.logistics.document.service.DocumentService;
import com.ithjja.logistics.web.utils.WebUtils;

/**
 * @author hejja
 */
@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentMapper documentMapper;

    @Override
    public void add(List<Document> documents) {
        for (Document document : documents) {
            document.setCreateTime(new Date());
            document.setUploadUser(WebUtils.getUserId());
            documentMapper.add(document);
        }
    }

    @Override
    public List<Document> findByModuleAndTypeAndEntity(Integer module, Integer type, Long entity) {
        return documentMapper.findByModuleAndTypeAndEntity(module, type, entity);
    }
}
