package com.ithjja.logistics.document.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.ithjja.logistics.document.model.Document;

/**
 * @author hejja
 */
@Mapper
public interface DocumentMapper {

    /**
     * 添加文件
     * @param document 文件
     */
    @Insert("insert into tbl_document values(#{id}, #{entity}, #{filename}, #{path}, #{module}, #{type}, "
        + "#{createTime}, #{uploadUser})")
    void add(Document document);

    /**
     * 根据模块、类型、实体查询文件
     * @param module 模块
     * @param type 类型
     * @param entity 实体id
     * @return 文件集合
     */
    @Select("select * from tbl_document where module = #{module} and type = #{type} and entity = #{entity}")
    @Results(id = "defaultResultMapping", value = {
        @Result(id = true, column = "id", property = "id"),
        @Result(column = "entity", property = "entity"),
        @Result(column = "filename", property = "filename"),
        @Result(column = "path", property = "path"),
        @Result(column = "module", property = "module"),
        @Result(column = "type", property = "type"),
        @Result(column = "create_time", property = "createTime"),
        @Result(column = "upload_user", property = "uploadUser")
    })
    List<Document> findByModuleAndTypeAndEntity(@Param("module") Integer module, @Param("type") Integer type,
        @Param("entity") Long entity);

}
