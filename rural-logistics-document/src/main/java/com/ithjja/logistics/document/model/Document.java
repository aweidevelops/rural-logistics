package com.ithjja.logistics.document.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author hejja
 */
@Data
public class Document implements Serializable {

    private static final long serialVersionUID = 4212159011111619890L;

    private Long id;

    private Long entity;

    private String filename;

    private String path;

    private Integer module;

    private Integer type;

    private Date createTime;

    private Long uploadUser;
}
