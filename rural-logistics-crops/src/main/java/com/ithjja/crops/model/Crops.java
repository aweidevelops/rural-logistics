package com.ithjja.crops.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.ithjja.logistics.common.model.Document;

/**
 * @author hejja
 */
@Data
public class Crops implements Serializable {

    private static final long serialVersionUID = -1455761694063007185L;

    private Long id;

    private String name;

    private Boolean deleted = Boolean.FALSE;

    private Date createTime;

    private List<Document> documents;
}
