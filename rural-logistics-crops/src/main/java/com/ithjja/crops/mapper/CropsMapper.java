package com.ithjja.crops.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ithjja.crops.model.Crops;

/**
 * @author hejja
 */
@Mapper
public interface CropsMapper {

    /**
     * 添加农作物
     * @param crops 农作物信息
     */
    @Insert("insert into tbl_crops values(#{id}, #{name}, #{deleted}, #{createTime})")
    void addCrops(Crops crops);

    /**
     * 修改农作物名称
     * @param name 农作物名称
     * @param id 农作物id
     */
    @Update("update tbl_crops set name = #{name} where id = #{id}")
    void updateCrops(@Param("name") String name, @Param("id") Long id);

    /**
     * 根据农作物id删除农作物
     * @param id 农作物id
     */
    @Update("update tbl_crops set deleted = true where id = #{id}")
    void deleteById(@Param("id") Long id);

    /**
     * 查询所有的农作物信息
     * @return 农作物集合
     */
    @Select("select * from tbl_crops where deleted = false")
    @Results(id = "defaultResultMapping", value = {
        @Result(id = true, column = "id", property = "id"),
        @Result(column = "name", property = "name"),
        @Result(column = "deleted", property = "deleted"),
        @Result(column = "create_time", property = "createTime")
    })
    List<Crops> findAll();

    /**
     * 根据id查询农作物
     * @param id 农作物id
     * @return 农作物信息
     */
    @Select("select * from tbl_crops where id = #{id}")
    @ResultMap("defaultResultMapping")
    Crops findById(@Param("id") Long id);

    @Select("select * from tbl_crops where id in (#{ids})")
    List<Crops> findNameByIds(@Param("ids") List<Long> ids);
}
