package com.ithjja.crops.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjja.crops.feign.DocumentFeign;
import com.ithjja.crops.mapper.CropsMapper;
import com.ithjja.crops.model.Crops;
import com.ithjja.crops.service.CropsService;
import com.ithjja.logistics.common.consts.DocumentConsts;
import com.ithjja.logistics.common.consts.ModuleConsts;

/**
 * @author hejja
 */
@Service
public class CropsServiceImpl implements CropsService {

    @Autowired
    private CropsMapper cropsMapper;

    @Autowired
    private DocumentFeign documentFeign;

    @Override
    public void addCrops(Crops crops) {
        crops.setCreateTime(new Date());
        cropsMapper.addCrops(crops);
    }

    @Override
    public void updateCrops(String name, Long id) {
        cropsMapper.updateCrops(name, id);
    }

    @Override
    public void deleteById(Long id) {
        cropsMapper.deleteById(id);
    }

    @Override
    public List<Crops> findAll() {
        return cropsMapper.findAll();
    }

    @Override
    public Crops findById(Long id) {
        return cropsMapper.findById(id);
    }

    @Override
    public List<Crops> findNameByIds(List<Long> ids) {
        List<Crops> crops = new ArrayList<>();
        for (Long id : ids) {
            crops.add(cropsMapper.findById(id));
        }
        return crops;
    }

    private void findDocument(List<Crops> crops) {
        for (Crops crop : crops) {
            crop.setDocuments(documentFeign.findDocument(ModuleConsts.CROPS_MODULE,
                DocumentConsts.CROPS_TYPE, crop.getId()).getBody());
        }
    }
}
