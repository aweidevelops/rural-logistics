package com.ithjja.crops.service;

import java.util.List;

import com.ithjja.crops.model.Crops;

/**
 * @author hejja
 */
public interface CropsService {

    /**
     * 添加农作物
     * @param crops 农作物信息
     */
    void addCrops(Crops crops);

    /**
     * 修改农作物名称
     * @param name 农作物名称
     * @param id 农作物id
     */
    void updateCrops(String name, Long id);

    /**
     * 根据农作物id删除农作物
     * @param id 农作物id
     */
    void deleteById(Long id);

    /**
     * 查询所有的农作物信息
     * @return 农作物集合
     */
    List<Crops> findAll();

    /**
     * 根据id查询农作物
     * @param id 农作物id
     * @return 农作物信息
     */
    Crops findById(Long id);

    List<Crops> findNameByIds(List<Long> ids);
}
