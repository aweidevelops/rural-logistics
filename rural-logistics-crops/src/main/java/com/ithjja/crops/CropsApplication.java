package com.ithjja.crops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author hejja
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class CropsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CropsApplication.class, args);
    }
}
