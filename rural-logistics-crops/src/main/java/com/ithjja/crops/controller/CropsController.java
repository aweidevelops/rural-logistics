package com.ithjja.crops.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ithjja.crops.model.Crops;
import com.ithjja.crops.service.CropsService;

/**
 * @author hejja
 */
@RestController
@RequestMapping("/api/crops")
public class CropsController {

    @Autowired
    private CropsService cropsService;

    /**
     * 添加农作物
     * @param crops 农作物信息
     * @return 响应
     */
    @PutMapping("/add")
    public ResponseEntity<?> add(@RequestBody Crops crops) {
        cropsService.addCrops(crops);
        return ResponseEntity.noContent().build();
    }

    /**
     * 修改农作物信息
     * @param crops 农作物信息
     * @return 响应
     */
    @PostMapping("/updateCrops")
    public ResponseEntity<?> updateCrops(@RequestBody Crops crops) {
        cropsService.updateCrops(crops.getName(), crops.getId());
        return ResponseEntity.noContent().build();
    }

    /**
     * 查询所有农作物
     * @return 农作物集合
     */
    @GetMapping("/findAll")
    public ResponseEntity<List<Crops>> findAll() {
        return ResponseEntity.ok(cropsService.findAll());
    }

    /**
     * 根据农作物id查询农作物
     * @param id 农作物id
     * @return 农作物
     */
    @GetMapping("/findById/{id}")
    public ResponseEntity<Crops> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(cropsService.findById(id));
    }

    @PostMapping("/findCrops")
    public ResponseEntity<List<Crops>> findName(@RequestBody List<Long> ids) {
        return ResponseEntity.ok(cropsService.findNameByIds(ids));
    }
}
