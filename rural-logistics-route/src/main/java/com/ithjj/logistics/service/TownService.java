package com.ithjj.logistics.service;

import java.util.List;

import com.ithjj.logistics.model.Town;

/**
 * @author hejja
 */
public interface TownService {

    /**
     * 添加城镇
     * @param town 城镇信息
     */
    void addTown(Town town);

    /**
     * 修改城镇信息
     * @param town 城镇信息
     */
    void updateTown(Town town);

    /**
     * 查询所有城镇信息
     * @return 城镇集合
     */
    List<Town> findAll();

    /**
     * 根据城市id查询城镇信息
     * @param cid 城市id
     * @return 城镇信息
     */
    List<Town> findByCid(Long cid);

    /**
     * 根据城镇id查询城镇信息
     * @param id 城镇id
     * @return 城镇信息
     */
    Town findById(Long id);
}
