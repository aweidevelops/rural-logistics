package com.ithjj.logistics.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjj.logistics.mapper.CityMapper;
import com.ithjj.logistics.model.City;
import com.ithjj.logistics.service.CityService;

/**
 * @author hejja
 */
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityMapper cityMapper;

    @Override
    public void addCity(City city) {
        city.setCreateTime(new Date());
        cityMapper.addCity(city);
    }

    @Override
    public void updateCity(City city) {
        cityMapper.updateCity(city);
    }

    @Override
    public List<City> findAll() {
        return cityMapper.findAll();
    }

    @Override
    public List<City> findByPid(Long pid) {
        return cityMapper.findByPid(pid);
    }

    @Override
    public City findById(Long id) {
        return cityMapper.findById(id);
    }
}
