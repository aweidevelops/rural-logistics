package com.ithjj.logistics.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjj.logistics.mapper.RouteMapper;
import com.ithjj.logistics.model.Route;
import com.ithjj.logistics.service.CityService;
import com.ithjj.logistics.service.ProvinceService;
import com.ithjj.logistics.service.RouteService;
import com.ithjj.logistics.service.TownService;
import com.ithjja.logistics.common.ex.ExpectedException;

/**
 * @author hejja
 */
@Service
public class RouteServiceImpl implements RouteService {

    @Autowired
    private RouteMapper routeMapper;

    @Autowired
    private ProvinceService provinceService;

    @Autowired
    private CityService cityService;

    @Autowired
    private TownService townService;


    @Override
    public void addRoute(Route route) {
        route.setCreateTime(new Date());
        routeMapper.addRoute(route);
    }

    @Override
    public Route findById(Long id) {
        Route route = routeMapper.findById(id);
        route.setEndPlaceStr(assemblePlace(route.getEndPlace()));
        route.setStartPlaceStr(route.getStartPlace());
        return route;
    }

    @Override
    public List<Route> findByUid(Long uid) {
        List<Route> routes = routeMapper.findByUid(uid);
        if (routes != null && !routes.isEmpty()) {
            routes.forEach(route -> {
                route.setEndPlaceStr(assemblePlace(route.getEndPlace()));
                route.setStartPlaceStr(route.getStartPlace());
            });
        }
        return routes;
    }

    @Override
    public void updateById(Route route) {
        routeMapper.updateById(route);
    }

    @Override
    public void deleteById(Long id) {
        routeMapper.deleteById(id);
    }

    /**
     * 组装省份、城市、城镇名
     * @param place 组装前的ids
     * @return 组装后的名称
     */
    private String assemblePlace(String place) {
        String[] split = place.split("-");
        if (split.length < 3) {
            throw new ExpectedException("错误！");
        }
        List<String> names = new ArrayList<>();
        // 省份名
        names.add(provinceService.findById(Long.valueOf(split[0])).getName());
        // 城市名
        names.add(cityService.findById(Long.valueOf(split[1])).getName());
        // 城镇名
        names.add(townService.findById(Long.valueOf(split[2])).getName());
        return StringUtils.join(names, "-");
    }
}
