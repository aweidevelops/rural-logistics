package com.ithjj.logistics.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjj.logistics.mapper.ProvinceMapper;
import com.ithjj.logistics.model.Province;
import com.ithjj.logistics.service.ProvinceService;

/**
 * @author hejja
 */
@Service
public class ProvinceServiceImpl implements ProvinceService {

    @Autowired
    private ProvinceMapper provinceMapper;

    @Override
    public void addProvince(Province province) {
        province.setCreateTime(new Date());
        provinceMapper.addProvince(province);
    }

    @Override
    public void updateById(String name, Long id) {
        provinceMapper.updateById(name, id);
    }

    @Override
    public List<Province> findProvinceAll() {
        return provinceMapper.findProvinceAll();
    }

    @Override
    public List<Province> findAll() {
        return provinceMapper.findAll();
    }

    @Override
    public Province findById(Long id) {
        return provinceMapper.findById(id);
    }
}
