package com.ithjj.logistics.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjj.logistics.mapper.TownMapper;
import com.ithjj.logistics.model.Town;
import com.ithjj.logistics.service.TownService;

/**
 * @author hejja
 */
@Service
public class TownServiceImpl implements TownService {

    @Autowired
    private TownMapper townMapper;

    @Override
    public void addTown(Town town) {
        town.setCreateTime(new Date());
        townMapper.addTown(town);
    }

    @Override
    public void updateTown(Town town) {
        townMapper.updateTown(town);
    }

    @Override
    public List<Town> findAll() {
        return townMapper.findAll();
    }

    @Override
    public List<Town> findByCid(Long cid) {
        return townMapper.findByCid(cid);
    }

    @Override
    public Town findById(Long id) {
        return townMapper.findById(id);
    }
}
