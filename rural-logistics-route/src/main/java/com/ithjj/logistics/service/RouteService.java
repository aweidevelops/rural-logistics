package com.ithjj.logistics.service;

import java.util.List;

import com.ithjj.logistics.model.Route;

/**
 * @author hejja
 */
public interface RouteService {

    /**
     * 添加路线
     * @param route 路线信息
     */
    void addRoute(Route route);

    /**
     * 根据id查询路线信息
     * @param id 路线id
     * @return 路线信息
     */
    Route findById(Long id);

    /**
     * 根据用户id查询路线
     * @param uid 用户id
     * @return 路线集合
     */
    List<Route> findByUid(Long uid);

    /**
     * 修改路线
     * @param route 路线
     */
    void updateById(Route route);

    /**
     * 删除路线
     * @param id id
     */
    void deleteById(Long id);

}
