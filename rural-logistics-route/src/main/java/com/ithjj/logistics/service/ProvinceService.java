package com.ithjj.logistics.service;

import java.util.List;

import com.ithjj.logistics.model.Province;

/**
 * @author hejja
 */
public interface ProvinceService {

    /**
     * 添加省份
     * @param province 省份信息
     */
    void addProvince(Province province);

    /**
     * 修改省份名
     * @param name 省份名
     * @param id 省份id
     */
    void updateById(String name, Long id);

    /**
     * 查询所有省份
     * @return 省份集合
     */
    List<Province> findProvinceAll();

    /**
     * 查询所有省份, 级联所有城市与城镇
     * @return 省份集合
     */
    List<Province> findAll();

    /**
     * 根据id查询省份信息
     * @param id 省份id
     * @return 省份信息
     */
    Province findById(Long id);
}
