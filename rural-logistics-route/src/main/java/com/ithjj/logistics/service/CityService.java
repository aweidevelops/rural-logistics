package com.ithjj.logistics.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ithjj.logistics.model.City;

/**
 * @author hejja
 */
public interface CityService {

    /**
     * 添加城市
     * @param city 城市
     */
    void addCity(City city);

    /**
     * 修改城市
     * @param city 城市信息
     */
    void updateCity(City city);

    /**
     * 查询所有城市信息
     * @return 城市集合
     */
    List<City> findAll();

    /**
     * 根据省份id查询城市信息, 级联城镇信息
     * @param pid 省份id
     * @return 城市集合
     */
    List<City> findByPid(Long pid);

    /**
     * 根据城市id查询城市信息
     * @param id 城市id
     * @return 城市信息
     */
    City findById(Long id);
}
