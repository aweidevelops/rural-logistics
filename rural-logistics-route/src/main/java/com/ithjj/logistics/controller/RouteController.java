package com.ithjj.logistics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ithjj.logistics.model.Route;
import com.ithjj.logistics.service.RouteService;
import com.ithjja.logistics.web.utils.WebUtils;

/**
 * @author hejja
 */
@RestController
@RequestMapping("/api/route")
public class RouteController {

    @Autowired
    private RouteService routeService;

    /**
     * 添加路线
     * @param route 路线信息
     * @return 响应
     */
    @PutMapping("/add")
    public ResponseEntity<?> addRoute(@RequestBody Route route) {
        routeService.addRoute(route);
        return ResponseEntity.noContent().build();
    }

    /**
     * 查询当前用户的路线
     * @return 当前用户的路线集合
     */
    @GetMapping("/findCurrentUser")
    public ResponseEntity<List<Route>> currentUserRoute() {
        return ResponseEntity.ok(routeService.findByUid(WebUtils.getUserId()));
    }

    /**
     * 删除路线
     * @param id 路线id
     * @return 响应
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        routeService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * 修改路线信息
     * @param route 路线信息
     * @return 响应
     */
    @PostMapping("/updateRoute")
    public ResponseEntity<?> updateRoute(@RequestBody Route route) {
        routeService.updateById(route);
        return ResponseEntity.noContent().build();
    }
}
