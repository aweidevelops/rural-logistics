package com.ithjj.logistics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ithjj.logistics.model.Province;
import com.ithjj.logistics.service.ProvinceService;

/**
 * @author hejja
 */
@RestController
@RequestMapping("/api/province")
public class ProvinceController {

    @Autowired
    private ProvinceService provinceService;

    /**
     * 添加省份
     * @param province 省份信息
     * @return 响应
     */
    @PutMapping("/add")
    public ResponseEntity<?> addProvince(@RequestBody Province province) {
        provinceService.addProvince(province);
        return ResponseEntity.noContent().build();
    }

    /**
     * 修改省份信息
     * @param province 省份信息
     * @return 响应
     */
    @PostMapping("/updateProvince")
    public ResponseEntity<?> updateProvince(@RequestBody Province province) {
        provinceService.updateById(province.getName(), province.getId());
        return ResponseEntity.noContent().build();
    }

    /**
     * 查询所有省份信息
     * @return 省份集合
     */
    @GetMapping("/findProvinceAll")
    public ResponseEntity<List<Province>> findProvinceAll() {
        return ResponseEntity.ok(provinceService.findProvinceAll());
    }

    /**
     * 查询所有省份信息，包含城市、城镇
     * @return 集合
     */
    @GetMapping("/findAll")
    public ResponseEntity<List<Province>> findAll() {
        return ResponseEntity.ok(provinceService.findAll());
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Province> findProvinceById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(provinceService.findById(id));
    }
}
