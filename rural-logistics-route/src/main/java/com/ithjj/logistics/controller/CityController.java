package com.ithjj.logistics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ithjj.logistics.model.City;
import com.ithjj.logistics.service.CityService;

/**
 * @author hejja
 */
@RestController
@RequestMapping("/api/city")
public class CityController {

    @Autowired
    private CityService cityService;

    /**
     * 添加城市信息
     * @param city 城市信息
     * @return 响应
     */
    @PutMapping("/add")
    public ResponseEntity<?> add(@RequestBody City city) {
        cityService.addCity(city);
        return ResponseEntity.noContent().build();
    }

    /**
     * 修改城市信息
     * @param city 城市信息
     * @return 响应
     */
    @PostMapping("/updateCity")
    public ResponseEntity<?> updateCity(@RequestBody City city) {
        cityService.updateCity(city);
        return ResponseEntity.noContent().build();
    }

    /**
     * 查询所有城市信息
     * @return 城市集合
     */
    @GetMapping("/findAll")
    public ResponseEntity<List<City>> findAll() {
        return ResponseEntity.ok(cityService.findAll());
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<City> findCityById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(cityService.findById(id));
    }
}
