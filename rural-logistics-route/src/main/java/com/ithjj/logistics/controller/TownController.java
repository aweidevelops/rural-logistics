package com.ithjj.logistics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ithjj.logistics.model.Town;
import com.ithjj.logistics.service.TownService;

/**
 * @author hejja
 */
@RestController
@RequestMapping("/api/town")
public class TownController {

    @Autowired
    private TownService townService;

    /**
     * 添加城镇
     * @param town 城镇信息
     * @return 响应
     */
    @PutMapping("/add")
    public ResponseEntity<?> add(@RequestBody Town town) {
        townService.addTown(town);
        return ResponseEntity.noContent().build();
    }

    /**
     * 修改城镇信息
     * @param town 城镇信息
     * @return 响应
     */
    @PostMapping("/updateTown")
    public ResponseEntity<?> updateTown(@RequestBody Town town) {
        townService.updateTown(town);
        return ResponseEntity.noContent().build();
    }

    /**
     * 查询所有城镇信息
     * @return 城镇集合
     */
    @GetMapping("/findAll")
    public ResponseEntity<List<Town>> findAll() {
        return ResponseEntity.ok(townService.findAll());
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Town> findTownById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(townService.findById(id));
    }
}
