package com.ithjj.logistics.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author hejja
 */
@Data
public class Route implements Serializable {

    private static final long serialVersionUID = -7878286146033945503L;

    private Long id;

    private Long uid;

    private String startPlace;

    private String startPlaceStr;

    private String endPlace;

    private String endPlaceStr;

    private Boolean deleted = Boolean.FALSE;

    private Date createTime;

    private Integer status;

    private Double maxWeight;

}
