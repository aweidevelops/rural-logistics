package com.ithjj.logistics.consts;

/**
 * 路线的常量类
 * @author hejja
 */
public class RouteConsts {

    /** 路线未出发 */
    public static final Integer ROUTE_STATUS_1 = 1;

    /** 路线状态已出发 */
    public static final Integer ROUTE_STATUS_2 = 2;
}
