package com.ithjj.logistics.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ithjj.logistics.model.Route;

/**
 * @author hejja
 */
@Mapper
public interface RouteMapper {

    /**
     * 添加路线
     * @param route 路线信息
     */
    @Insert("insert into tbl_route values(#{id}, #{uid}, #{startPlace}, #{endPlace}, #{deleted}, "
        + " #{createTime}, #{status})")
    void addRoute(Route route);

    /**
     * 根据id查询路线信息
     * @param id 路线id
     * @return 路线信息
     */
    @Select("select * from tbl_route where id = #{id} where deletd = false")
    @Results(id = "defaultResultMapping", value = {
        @Result(id = true, column = "id", property = "id"),
        @Result(column = "uid", property = "uid"),
        @Result(column = "start_place", property = "startPlace"),
        @Result(column = "end_place", property = "endPlace"),
        @Result(column = "deleted", property = "deleted"),
        @Result(column = "create_time", property = "createTime"),
        @Result(column = "status", property = "status"),
    })
    Route findById(@Param("id") Long id);

    /**
     * 根据用户id查询路线
     * @param uid 用户id
     * @return 路线集合
     */
    @Select("select * from tbl_route where uid = #{uid} and deleted = false")
    @ResultMap("defaultResultMapping")
    List<Route> findByUid(@Param("uid") Long uid);

    /**
     * 修改路线
     * @param route 路线
     */
    @Update("update tbl_route set start_place = #{startPlace}, end_place = #{endPlace} where id = #{id}")
    void updateById(Route route);

    /**
     * 删除路线
     * @param id id
     */
    @Update("update tbl_route set deleted = true where id = #{id}")
    void deleteById(@Param("id") Long id);
}
