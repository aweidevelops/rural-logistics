package com.ithjj.logistics.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ithjj.logistics.model.City;

/**
 * @author hejja
 */
@Mapper
public interface CityMapper {

    /**
     * 添加城市
     * @param city 城市
     */
    @Insert("insert into tbl_city values(#{id}, #{pid}, #{name}, #{deleted}, #{createTime})")
    void addCity(City city);

    /**
     * 修改城市
     * @param city 城市信息
     */
    @Update("update tbl_city set pid = #{pid}, name = #{name} where id= #{id}")
    void updateCity(City city);

    /**
     * 查询所有城市信息
     * @return 城市集合
     */
    @Select("select * from tbl_city where deleted = false")
    @ResultMap("provinceResultMapping")
    List<City> findAll();

    /**
     * 根据省份id查询城市信息, 级联城镇信息
     * @param pid 省份id
     * @return 城市集合
     */
    @Select("select * from tbl_city where pid = #{pid} and deleted = false")
    @Results(id = "townResultMapping", value = {
        @Result(id = true, column = "id", property = "id"),
        @Result(column = "pid", property = "pid"),
        @Result(column = "name", property = "name"),
        @Result(column = "deleted", property = "deleted"),
        @Result(column = "create_time", property = "createTime"),
        @Result(column = "id", property = "towns",
            many = @Many(select = "com.ithjj.logistics.mapper.TownMapper.findByCid"))
    })
    List<City> findByPid(@Param("pid") Long pid);

    /**
     * 根据城市id查询城市信息
     * @param id 城市id
     * @return 城市信息
     */
    @Select("select * from tbl_city where id = #{id}")
    @Results(id = "provinceResultMapping", value = {
        @Result(id = true, column = "id", property = "id"),
        @Result(column = "pid", property = "pid"),
        @Result(column = "name", property = "name"),
        @Result(column = "deleted", property = "deleted"),
        @Result(column = "create_time", property = "createTime"),
        @Result(column = "pid", property = "province",
            one = @One(select = "com.ithjj.logistics.mapper.ProvinceMapper.findById"))
    })
    City findById(@Param("id") Long id);

    @Select("select * from tbl_city where id = #{id}")
    @Results(id = "defaultResultMapping", value = {
        @Result(id = true, column = "id", property = "id"),
        @Result(column = "pid", property = "pid"),
        @Result(column = "name", property = "name"),
        @Result(column = "deleted", property = "deleted"),
        @Result(column = "create_time", property = "createTime")
    })
    City findByIdDefault(@Param("id") Long id);
}
