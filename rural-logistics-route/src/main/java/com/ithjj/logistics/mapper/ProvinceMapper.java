package com.ithjj.logistics.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ithjj.logistics.model.Province;

/**
 * @author hejja
 */
@Mapper
public interface ProvinceMapper {

    /**
     * 添加省份
     * @param province 省份信息
     */
    @Insert("insert into tbl_province values(#{id}, #{name}, #{deleted}, #{createTime})")
    void addProvince(Province province);

    /**
     * 修改省份名
     * @param name 省份名
     * @param id 省份id
     */
    @Update("update tbl_province set name = #{name} where id = #{id}")
    void updateById(@Param("name") String name, @Param("id") Long id);

    /**
     * 查询所有省份
     * @return 省份集合
     */
    @Select("select * from tbl_province where deleted = false")
    @Results(id = "defaultResultMapping", value = {
        @Result(id = true, column = "id", property = "id"),
        @Result(column = "name", property = "name"),
        @Result(column = "deleted", property = "deleted"),
        @Result(column = "create_time", property = "createTime")
    })
    List<Province> findProvinceAll();

    /**
     * 查询所有省份, 级联所有城市与城镇
     * @return 省份集合
     */
    @Select("select * from tbl_province where deleted = false")
    @Results(id = "cityAndTownResultMapping", value = {
        @Result(id = true, column = "id", property = "id"),
        @Result(column = "name", property = "name"),
        @Result(column = "deleted", property = "deleted"),
        @Result(column = "create_time", property = "createTime"),
        @Result(column = "id", property = "cities",
            many = @Many(select = "com.ithjj.logistics.mapper.CityMapper.findByPid"))
    })
    List<Province> findAll();

    /**
     * 根据id查询省份信息
     * @param id 省份id
     * @return 省份信息
     */
    @Select("select * from tbl_province where id = #{id}")
    @ResultMap("defaultResultMapping")
    Province findById(@Param("id") Long id);
}
