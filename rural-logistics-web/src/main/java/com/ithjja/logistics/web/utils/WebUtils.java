package com.ithjja.logistics.web.utils;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.util.Strings;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.ithjja.logistics.common.consts.CommonConsts;

/**
 * @author hejja
 */
public class WebUtils extends org.springframework.web.util.WebUtils {

    public static HttpServletRequest getHttpServletRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    public static JSONObject getJwtPayload() {
        return JSONUtil.parseObj(getHttpServletRequest().getHeader(CommonConsts.JWT_PAYLOAD_KEY));
    }

    public static Long getUserId() {
        return getJwtPayload().getLong("id");
    }

    public static String getJWT() {
        if (null == getHttpServletRequest().getHeader(CommonConsts.JWT_TOKEN_HEADER)) {
            return "";
        }
        return getHttpServletRequest().getHeader(CommonConsts.JWT_TOKEN_HEADER).replace(CommonConsts.JWT_TOKEN_PREFIX,
            Strings.EMPTY);
    }
}
