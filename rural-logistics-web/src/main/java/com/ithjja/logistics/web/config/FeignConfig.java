package com.ithjja.logistics.web.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;

import org.springframework.http.HttpHeaders;

import com.ithjja.logistics.common.consts.CommonConsts;
import com.ithjja.logistics.web.utils.WebUtils;

/**
 * @author hejja
 */
public class FeignConfig implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header(HttpHeaders.AUTHORIZATION, CommonConsts.JWT_TOKEN_PREFIX.concat(WebUtils.getJWT()));
    }
}
