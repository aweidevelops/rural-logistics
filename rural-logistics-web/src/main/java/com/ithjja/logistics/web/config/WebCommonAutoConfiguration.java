package com.ithjja.logistics.web.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ithjja.logistics.web.advice.WebExceptionHandler;

/**
 * @author hejja
 */
@Configuration(proxyBeanMethods = false)
public class WebCommonAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public WebExceptionHandler webExceptionHandler() {
        return new WebExceptionHandler();
    }
}
