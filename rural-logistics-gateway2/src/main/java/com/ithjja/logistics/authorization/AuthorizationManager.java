package com.ithjja.logistics.authorization;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.ReactiveAuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.server.authorization.AuthorizationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import com.ithjja.logistics.common.consts.CommonConsts;
import com.ithjja.logistics.common.consts.RedisConsts;

/**
 * @author hejja
 */
@Component
@Slf4j
public class AuthorizationManager implements ReactiveAuthorizationManager<AuthorizationContext> {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public Mono<AuthorizationDecision> check(Mono<Authentication> mono, AuthorizationContext authorizationContext) {

        ServerHttpRequest request = authorizationContext.getExchange().getRequest();
        String path = request.getURI().getPath();
        String method = request.getMethod().name();
        PathMatcher pathMatcher = new AntPathMatcher();

        // 对应跨域的预检请求放行
        if (request.getMethod() == HttpMethod.OPTIONS) {
            return Mono.just(new AuthorizationDecision(true));
        }

        // token为空拒绝访问
        String token = request.getHeaders().getFirst(CommonConsts.JWT_TOKEN_HEADER);
        if (StrUtil.isBlank(token)) {
            return Mono.just(new AuthorizationDecision(false));
        }

        Object obj = redisTemplate.opsForHash().get(RedisConsts.RKEY_API_PERMIS, path);
        Map<Object, Object> apiPermis = redisTemplate.opsForHash().entries(RedisConsts.RKEY_API_PERMIS);
        Iterator<Object> iterator = apiPermis.keySet().iterator();
        List<String> authorities = new ArrayList<>();
        while (iterator.hasNext()) {
            String pattern = (String) iterator.next();
            if (pathMatcher.match(pattern, path)) {
                authorities.add(apiPermis.get(pattern).toString());
            }
        }
//        authorities = authorities.stream().map(i -> i = CommonConsts.AUTHORITY_PREFIX + i).collect(Collectors.toList());

        return mono
            .filter(Authentication::isAuthenticated)
            .flatMapIterable(Authentication::getAuthorities)
            .map(GrantedAuthority::getAuthority)
            .any(role -> {
                log.info("访问路径：" + path);
                log.info("用户角色信息：" + role);
                log.info("资源需要权限：" + authorities.toString());
                log.info("是否有权限：" + authorities.contains(role));
                return authorities.contains(role);
            })
            .map(AuthorizationDecision::new)
            .defaultIfEmpty(new AuthorizationDecision(false));
    }
}
