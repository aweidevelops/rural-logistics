package com.ithjja.logistics.config;

import lombok.Data;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author hejja
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "logistics.whitelist")
public class WhiteListConfig {

    /** 白名单 */
    private List<String> urls;
}
