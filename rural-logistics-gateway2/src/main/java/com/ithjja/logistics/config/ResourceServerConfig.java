package com.ithjja.logistics.config;

import cn.hutool.core.util.ArrayUtil;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtAuthenticationConverterAdapter;
import org.springframework.security.web.server.SecurityWebFilterChain;

import com.ithjja.logistics.authorization.AuthorizationManager;
import com.ithjja.logistics.common.consts.CommonConsts;
import com.ithjja.logistics.component.RestAuthenticationEntryPoint;
import com.ithjja.logistics.component.RestfulAccessDeniedHandler;

/**
 * 资源服务器配置
 * @author hejja
 */
@AllArgsConstructor
@Configuration
@EnableWebFluxSecurity
public class ResourceServerConfig {

    /** 认证中心 */
    private final AuthorizationManager authorizationManager;
    /** 白名单 */
    private final WhiteListConfig whiteListConfig;
    /** 没有权限访问时处理 */
    private final RestfulAccessDeniedHandler restfulAccessDeniedHandler;
    /** token失效处理 */
    private final RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http.oauth2ResourceServer().jwt()
            .jwtAuthenticationConverter(jwtAuthenticationConverter());
        // 自定义处理JWT请求头过期或签名错误的结果
        http.oauth2ResourceServer().authenticationEntryPoint(restAuthenticationEntryPoint);
        http.authorizeExchange()
            .pathMatchers(ArrayUtil.toArray(whiteListConfig.getUrls(),String.class)).permitAll()//白名单配置
            .anyExchange().access(authorizationManager)//鉴权管理器配置
            .and().exceptionHandling()
            .accessDeniedHandler(restfulAccessDeniedHandler)//处理未授权
            .authenticationEntryPoint(restAuthenticationEntryPoint)//处理未认证
            .and().csrf().disable();
        return http.build();
    }

    // JWT处理
    @Bean
    public Converter<Jwt, ? extends Mono<? extends AbstractAuthenticationToken>> jwtAuthenticationConverter() {
        JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        jwtGrantedAuthoritiesConverter.setAuthorityPrefix(CommonConsts.AUTHORITY_PREFIX);
        jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName(CommonConsts.AUTHORITY_CLAIM_NAME);

        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
        return new ReactiveJwtAuthenticationConverterAdapter(jwtAuthenticationConverter);
    }
}
