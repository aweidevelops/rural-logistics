package com.ithjja.logistics.filter;

import cn.hutool.core.util.StrUtil;
import lombok.SneakyThrows;
import reactor.core.publisher.Mono;

import org.apache.logging.log4j.util.Strings;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import com.ithjja.logistics.common.consts.CommonConsts;
import com.nimbusds.jose.JWSObject;

/**
 * 将登陆用户的JWT转化成用户信息的全局过滤器
 * @author hejja
 */
@Component
public class AuthGlobalFilter implements GlobalFilter, Ordered {

    @Override
    @SneakyThrows
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String token = exchange.getRequest().getHeaders().getFirst(CommonConsts.JWT_TOKEN_HEADER);
        if (StrUtil.isEmpty(token)) {
            return chain.filter(exchange);
        }
        //从token中解析用户信息并设置到Header中去
        JWSObject jwsObject = JWSObject.parse(token.replace(CommonConsts.JWT_TOKEN_PREFIX, Strings.EMPTY));
        // JWT中用户信息
        String payload = jwsObject.getPayload().toString();
        ServerHttpRequest request = exchange.getRequest().mutate()
            .header(CommonConsts.JWT_PAYLOAD_KEY, payload)
            .build();
        exchange = exchange.mutate().request(request).build();
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
