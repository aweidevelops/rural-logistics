package com.ithjja.logistics.common.consts;

/**
 * 用户常量
 * @author hejja
 */
public class UserInfoConsts {

    /** 禁用 */
    public static final Integer STATUS_2 = 2;

    /** 不可登陆 */
    public static final Integer STATUS_3 = 3;
}
