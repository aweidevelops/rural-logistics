package com.ithjja.logistics.common.consts;

/**
 * 模块的常量类
 * @author hejja
 */
public class ModuleConsts {

    /** 农作物模块 */
    public static final Integer CROPS_MODULE = 1;
}
