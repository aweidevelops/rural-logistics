package com.ithjja.logistics.common.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author hejja
 */
@Data
public class Town implements Serializable {

    private static final long serialVersionUID = -729223402816438662L;

    private Long id;

    private Long cid;

    private City city;

    private String name;

    private Boolean deleted = Boolean.FALSE;

    private Date createTime;
}
