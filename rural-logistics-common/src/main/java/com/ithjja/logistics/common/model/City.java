package com.ithjja.logistics.common.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author hejja
 */
@Data
public class City implements Serializable {

    private static final long serialVersionUID = -3474273569599503404L;

    private Long id;

    private Long pid;

    private Province province;

    private String name;

    private Boolean deleted = Boolean.FALSE;

    private Date createTime;

    private List<Town> towns;
}
