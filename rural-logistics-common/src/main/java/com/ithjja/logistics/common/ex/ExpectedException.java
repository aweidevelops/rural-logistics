package com.ithjja.logistics.common.ex;

/**
 * @author hejja
 */
public class ExpectedException extends RuntimeException {

    private static final long serialVersionUID = -6246500729416300634L;

    public ExpectedException() {
        super();
    }

    public ExpectedException(String message) {
        super(message);
    }
}
