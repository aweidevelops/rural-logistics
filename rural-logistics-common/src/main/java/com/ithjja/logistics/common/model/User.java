package com.ithjja.logistics.common.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author hejja
 */
@Data
public class User implements Serializable {

    private static final long serialVersionUID = -407725664360931170L;

    private Long id;

    private String username;

    private String password;

    private String name;

    private String email;

    private Integer status;

    private Date createTime;

    private List<String> permissions;
}
