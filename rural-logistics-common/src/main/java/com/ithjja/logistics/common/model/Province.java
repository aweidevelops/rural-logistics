package com.ithjja.logistics.common.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author hejja
 */
@Data
public class Province implements Serializable {

    private static final long serialVersionUID = -1097921308557361842L;

    private Long id;

    private String name;

    private Boolean deleted = Boolean.FALSE;

    private Date createTime;

    private List<City> cities;
}
