package com.ithjja.logistics.common.consts;

/**
 * 公用常量类
 * @author hejja
 */
public class CommonConsts {

    public static final String JWT_TOKEN_HEADER = "Authorization";

    /** JWT存储权限前缀 */
    public static final String AUTHORITY_PREFIX = "ROLE_";

    /** JWT存储权限属性 */
    public static final String AUTHORITY_CLAIM_NAME = "authorities";

    /** JWT令牌前缀 Bearer */
    public static final String JWT_TOKEN_PREFIX = "Bearer ";

    /** JWT载体key */
    public static final String JWT_PAYLOAD_KEY = "payload";
}
