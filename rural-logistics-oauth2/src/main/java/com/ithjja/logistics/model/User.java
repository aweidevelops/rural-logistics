package com.ithjja.logistics.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0
 **/
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 5181027673882408493L;
    
    private Long id;

    private String username;

    private String password;

    private String name;

    private String email;

    private Integer status;

    private Date createTime;

    private List<String> permissions;
}
