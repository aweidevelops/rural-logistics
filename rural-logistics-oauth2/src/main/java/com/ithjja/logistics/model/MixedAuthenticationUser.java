package com.ithjja.logistics.model;

import java.util.Collection;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * @author hejja
 */
public class MixedAuthenticationUser extends User {

    /** 用户的ID */
    private Long userId;

    /** 用户姓名 */
    private String name;

    /** 身份认证模式 */
    private String authType;

    /** 身份认证的参数配置 */
    private Map<String, ?> authInfo;

    public MixedAuthenticationUser(Long userId, String username, String name, String password, boolean enabled,
        boolean accountNonExpired, boolean credentialsNonExpired, String authType,
        Map<String, ?> authInfo, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, true,
            authorities);
        this.userId = userId;
        this.name = name;
        this.authType = authType;
        this.authInfo = authInfo;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public Map<String, ?> getAuthInfo() {
        return authInfo;
    }

    public void setAuthInfo(Map<String, ?> authInfo) {
        this.authInfo = authInfo;
    }
}
