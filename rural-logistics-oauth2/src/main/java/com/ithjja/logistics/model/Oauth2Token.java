package com.ithjja.logistics.model;

import lombok.Builder;
import lombok.Data;

/**
 * @author hejja
 */
@Data
@Builder
public class Oauth2Token {

    private String accessToken;

    private String refreshToken;

    private String tokenHead;

    private int expiresIn;

    private String userId;
}
