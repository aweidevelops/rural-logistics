package com.ithjja.logistics.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.ithjja.logistics.model.Permission;

/**
 * @author hejja
 */
@Mapper
public interface PermissionMapper {

    @Select("select * from tbl_permission")
    @Results(id = "defaultResultMapping", value = {
        @Result(id = true, column = "id", property = "id"),
        @Result(column = "perm_name", property = "permName"),
        @Result(column = "perm_tag", property = "permTag"),
        @Result(column = "url", property = "url")
    })
    List<Permission> findAll();
}
