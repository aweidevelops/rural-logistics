package com.ithjja.logistics.mapper;

import com.ithjja.logistics.model.User;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0
 */
@Mapper
public interface UserMapper {

    @Select("select * from tbl_user where username = #{username}")
    @Results(id = "defaultResultMapping", value = {
        @Result(id = true, column = "id", property = "id"),
        @Result(column = "username", property = "username"),
        @Result(column = "name", property = "name"),
        @Result(column = "password", property = "password"),
        @Result(column = "email", property = "email"),
        @Result(column = "create_time", property = "createTime"),
        @Result(column = "status", property = "status")
    })
    User getUserByUsername(@Param("username") String username);

    @Select("select perm_tag from tbl_permission where id in "
        + " (select permission_id from tbl_role_permission where role_id in "
        + " (select role_id from tbl_user_role where user_id = #{userId}))")
    List<String> findPermissionsByUserId(@Param("userId") Long userId);

    @Select("select id, username, name, email, create_time, status from tbl_user where id = #{id}")
    @ResultMap("defaultResultMapping")
    User findById(@Param("id") Long id);

    @Select("select id, username, name, email, create_time, status from tbl_user where id = #{id}")
    @ResultMap("defaultResultMapping")
    User findUserById(@Param("id") Long id);
}
