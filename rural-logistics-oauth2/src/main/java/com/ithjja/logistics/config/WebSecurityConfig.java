package com.ithjja.logistics.config;

import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ithjja.logistics.service.MixedAuthenticationProvider;

/**
 * @author hejja
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    //认证管理器
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    //密码编码器
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    //安全拦截机制（最重要）
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests().requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll()
            .and()
            .authorizeRequests().antMatchers("/api/rsa/publicKey", "/oauth/logout", "/oauth/ping", "/api/user/**", "/oauth/test")
            .permitAll()
            .anyRequest().authenticated()
            .and()
            .csrf().disable();
    }

//    @Bean
//    public MixedAuthenticationProvider authenticationProvider() {
//        MixedAuthenticationProvider provider = new MixedAuthenticationProvider();
//        provider.setPostAuthenticationChecks(new AccountStatusUserDetailsChecker());
//        return provider;
//    }

}
