package com.ithjja.logistics.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ithjja.logistics.model.User;
import com.ithjja.logistics.service.UserService;
import com.ithjja.logistics.web.utils.WebUtils;

/**
 * @author hejja
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 获取当前用户信息，不含密码
     * @return 用户信息
     */
    @GetMapping("/current")
    public ResponseEntity<User> current() {
        return ResponseEntity.ok(userService.current(WebUtils.getUserId()));
    }

    @GetMapping("/getUserInfo/{id}")
    public ResponseEntity<User> getUserInfo(@PathVariable("id") Long id) {
        return ResponseEntity.ok(userService.getUserInfo(id));
    }

}
