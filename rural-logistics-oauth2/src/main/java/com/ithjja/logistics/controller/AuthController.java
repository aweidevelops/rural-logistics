package com.ithjja.logistics.controller;

import lombok.extern.slf4j.Slf4j;

import java.security.Principal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ithjja.logistics.common.consts.CommonConsts;
import com.ithjja.logistics.model.Oauth2Token;

/**
 * @author hejja
 */
@RestController
@RequestMapping("/oauth")
@Slf4j
public class AuthController {

    @Autowired
    private TokenEndpoint tokenEndpoint;

    @PostMapping("/token")
    public ResponseEntity<Oauth2Token> postAccessToken(Principal principal,
        @RequestParam Map<String, String> parameters) {
        OAuth2AccessToken token = null;
        try {
            token = tokenEndpoint.postAccessToken(principal, parameters).getBody();
        } catch (HttpRequestMethodNotSupportedException e) {
            if (log.isErrorEnabled()) {
                log.error("", e);
            }
        }
        Oauth2Token oauth2Token = Oauth2Token.builder()
            .accessToken(token.getValue())
            .refreshToken(token.getRefreshToken().getValue())
            .expiresIn(token.getExpiresIn())
            .expiresIn(token.getExpiresIn())
            .userId(token.getAdditionalInformation().get("id").toString())
            .tokenHead(CommonConsts.JWT_TOKEN_PREFIX)
            .build();
        return ResponseEntity.ok(oauth2Token);
    }

}
