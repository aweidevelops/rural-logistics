package com.ithjja.logistics.service;

import cn.hutool.core.collection.CollUtil;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.ithjja.logistics.common.consts.CommonConsts;
import com.ithjja.logistics.common.consts.RedisConsts;
import com.ithjja.logistics.model.Permission;

/**
 * @author hejja
 */
@Service
public class RedisServiceImpl {

    private Map<String, String> resourceRolesMap;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private PermissionService permissionService;

    @PostConstruct
    public void initData() {
        redisTemplate.delete(RedisConsts.RKEY_API_PERMIS);
        resourceRolesMap = new TreeMap<>();
        // 查询出所有的权限列表
        List<Permission> permissions = permissionService.findAll();
        if (permissions != null && !permissions.isEmpty()) {
            for (Permission permission : permissions) {
                resourceRolesMap.put(permission.getUrl(), CommonConsts.AUTHORITY_PREFIX + permission.getPermTag());
            }
        }
        redisTemplate.opsForHash().putAll(RedisConsts.RKEY_API_PERMIS, resourceRolesMap);
    }
}
