package com.ithjja.logistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjja.logistics.mapper.PermissionMapper;
import com.ithjja.logistics.model.Permission;
import com.ithjja.logistics.service.PermissionService;

/**
 * @author hejja
 */
@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public List<Permission> findAll() {
        return permissionMapper.findAll();
    }
}
