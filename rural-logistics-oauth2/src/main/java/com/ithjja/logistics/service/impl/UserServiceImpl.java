package com.ithjja.logistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjja.logistics.mapper.UserMapper;
import com.ithjja.logistics.model.User;
import com.ithjja.logistics.service.UserService;

/**
 * @author hejja
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUserByUsername(String username) {
        return userMapper.getUserByUsername(username);
    }

    @Override
    public List<String> findPermissionsByUserId(Long id) {
        return userMapper.findPermissionsByUserId(id);
    }

    @Override
    public User current(Long id) {
        User user = userMapper.findById(id);
        user.setPermissions(userMapper.findPermissionsByUserId(id));
        return user;
    }

    @Override
    public User getUserInfo(Long id) {
        return userMapper.findUserById(id);
    }
}
