package com.ithjja.logistics.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.ithjja.logistics.common.consts.UserInfoConsts;
import com.ithjja.logistics.model.MixedAuthenticationUser;
import com.ithjja.logistics.model.User;

/**
 * @author hejja
 */
@Component
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 用户名称
        username = username.trim();
        // 查询用户
        User user = userService.getUserByUsername(username);
        if (user == null) {
            //如果用户查不到，返回null，由provider来抛出异常
            return null;
        }
        //根据用户的id查询用户的权限
        List<String> permissions = userService.findPermissionsByUserId(user.getId());
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String permission : permissions) {
            authorities.add(new SimpleGrantedAuthority(permission));
        }
        return new MixedAuthenticationUser(user.getId(), user.getName(), user.getUsername(),
            user.getPassword() == null ? "" : user.getPassword(), !UserInfoConsts.STATUS_3.equals(user.getStatus()),
            !UserInfoConsts.STATUS_2.equals(user.getStatus()), true, "native",
            null, authorities);
    }
}
