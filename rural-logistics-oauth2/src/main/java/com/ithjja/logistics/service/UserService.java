package com.ithjja.logistics.service;

import java.util.List;

import com.ithjja.logistics.model.User;

/**
 * @author hejja
 */
public interface UserService {

    /**
     * 根据用户名查询用户信息
     * @param username 用户名
     * @return 用户信息
     */
    User getUserByUsername(String username);

    /**
     * 根据用户id查询用户权限
     * @param id 用户id
     * @return 权限列表
     */
    List<String> findPermissionsByUserId(Long id);

    /**
     * 获取当前用户信息
     * @param id 用户id
     * @return 用户信息
     */
    User current(Long id);

    User getUserInfo(Long id);
}
