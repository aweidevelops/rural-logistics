package com.ithjja.logistics.service;

import java.util.List;

import com.ithjja.logistics.model.Permission;

/**
 * @author hejja
 */
public interface PermissionService {

    List<Permission> findAll();
}
